// Retrieve an element from the webpage
const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')
// "document"- refers to the whole webpage;
// "querySelector" - used to select a specific object/html element from the document


/*
Alternatively, get element functions could be used to retrieve the elements.
	based on id = document.getElementById('<id>')
	based on class = document.getElementByClassName('<class>')
	based on tag = document.getElementByTagName('<tag>')
*/


//Performs an action when an event triggers
txtLastName.addEventListener('keyup', (event) =>{
	spanFullName.innerHTML = txtFirstName.value +" "+ txtLastName.value

/*	console.log(event.target);
	console.log(event.target.value);*/
})


txtFirstName.addEventListener('keyup', (event) =>{
	// spanFullName.innerHTML = txtFirstName.value;

	// event.target - contains the element where the event happened
	// event.target.value - gets the value of the input object
	console.log(event.target);
	console.log(event.target.value);
})
txtLastName.addEventListener('keyup', (event) =>{
	// spanFullName.innerHTML = txtFirstName.value;

	// event.target - contains the element where the event happened
	// event.target.value - gets the value of the input object
	console.log(event.target);
	console.log(event.target.value);
})